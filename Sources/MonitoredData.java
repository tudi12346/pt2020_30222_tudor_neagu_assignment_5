package com.assignment5;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class MonitoredData {

	private LocalDateTime startTime;
	private LocalDateTime endTime;
	private String activity;

	public MonitoredData(LocalDateTime start, LocalDateTime end, String activity) {
		this.setStartTime(start);
		this.setEndTime(end);
		this.setActivity(activity);
	}

	public MonitoredData() {
	}

	public LocalDateTime getStartTime() {
		return startTime;
	}

	public void setStartTime(LocalDateTime startTime) {
		this.startTime = startTime;
	}

	public LocalDateTime getEndTime() {
		return endTime;
	}

	public void setEndTime(LocalDateTime endTime) {
		this.endTime = endTime;
	}

	public String getActivity() {
		return activity;
	}

	public void setActivity(String activity) {
		this.activity = activity;
	}

	public String toString() {
		return this.getStartTime().format(DateTimeFormatter.ofPattern("MM/dd/yyyy HH:mm:ss")).toString() + "\t"
				+ this.getEndTime().format(DateTimeFormatter.ofPattern("MM/dd/yyyy HH:mm:ss")).toString() + "\t"
				+ this.getActivity();

	}

	public Integer get1() {
		return 1;
	}

	public Integer getDay() {
		return this.startTime.getDayOfYear();
	}

}
