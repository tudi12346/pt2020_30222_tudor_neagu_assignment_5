package com.assignment5;

import java.util.List;

public class App {

	public static void main(String[] args) {
		List<MonitoredData> data = TaskComputer.readData();
		TaskWriter.task1(TaskComputer.readData());
		TaskWriter.task2(TaskComputer.countDistinctDays(data));
		TaskWriter.task3(TaskComputer.computeActivitiesFrequence(data));
		TaskWriter.task4(TaskComputer.activityEachDay(data));
		TaskWriter.task5(TaskComputer.totalDurationOfEachActivity(data));
		TaskWriter.task6(TaskComputer.avgDurationSmallerThanFiveMin(data));

	}

}
