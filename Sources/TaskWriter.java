package com.assignment5;

import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class TaskWriter {
	private static final String TASK1_FILE_NAME = "task1.txt";
	private static final String TASK2_FILE_NAME = "task2.txt";
	private static final String TASK3_FILE_NAME = "task3.txt";
	private static final String TASK4_FILE_NAME = "task4.txt";
	private static final String TASK5_FILE_NAME = "task5.txt";
	private static final String TASK6_FILE_NAME = "task6.txt";

	public static void task1(List<MonitoredData> activities) {
		try {
			FileWriter task1 = new FileWriter(TASK1_FILE_NAME);
			for (MonitoredData a : activities)
				task1.write(a.toString() + "\n");

			task1.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void task2(Integer count) {
		try {
			FileWriter task2 = new FileWriter(TASK2_FILE_NAME);
			task2.write("Sunt " + count + " zile diferite.\n");
			task2.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void task3(Map<String, Integer> map) {
		try {
			FileWriter task3 = new FileWriter(TASK3_FILE_NAME);
			for (Entry<String, Integer> entry : map.entrySet())
				task3.write(entry.getKey() + ":" + entry.getValue() + "\n");
			task3.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void task4(Map<Integer, Map<String, Integer>> map) {
		try {
			FileWriter task4 = new FileWriter(TASK4_FILE_NAME);
			task4.write("Results shown by day of the year:");
			for (Entry<Integer, Map<String, Integer>> i : map.entrySet()) {
				task4.write("\nDay:" + i.getKey() + "\n");
				for (Entry<String, Integer> j : i.getValue().entrySet()) {
					task4.write(j.getKey() + ":" + j.getValue() + "\t");
				}
			}
			task4.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void task5(Map<String, Long> map) {
		try {
			FileWriter task5 = new FileWriter(TASK5_FILE_NAME);
			long h, m, s, dur;
			for (Entry<String, Long> i : map.entrySet()) {
				dur = i.getValue() / 1000;
				h = dur / 3600;
				m = (dur % 3600) / 60;
				s = dur % 60;
				task5.write(i.getKey() + ": " + h + ":hrs " + m + ":min " + s + ":sec" + "\n");
			}
			task5.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void task6(List<String> list) {
		try {
			FileWriter task6 = new FileWriter(TASK6_FILE_NAME);
			for (String i : list) {
				task6.write(i + " ");
			}
			task6.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
