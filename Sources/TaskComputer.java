package com.assignment5;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class TaskComputer {
	public static final String DATA_FILE_NAME = "Activities.txt";

	// task1
	public static List<MonitoredData> readData() {
		String fileName = DATA_FILE_NAME;
		List<MonitoredData> activities = new ArrayList<MonitoredData>();

		try {
			Stream<String> stream = Files.lines(Paths.get(fileName));
			activities = stream.map(StringParser::stringToMonitoredData).collect(Collectors.toList());
			stream.close();
			return activities;
		} catch (IOException e) {
			e.printStackTrace();
		}

		return null;
	}

	public static <T> Predicate<T> distinctByKey(Function<? super T, ?> keyExtractor) {

		Map<Object, Boolean> seen = new ConcurrentHashMap<>();
		return t -> seen.putIfAbsent(keyExtractor.apply(t), Boolean.TRUE) == null;
	}

	// task2
	public static Integer countDistinctDays(List<MonitoredData> activities) {
		return activities.stream().filter(distinctByKey(p -> p.getStartTime().getDayOfYear()))
				.collect(Collectors.toList()).size();
	}

	// task3
	public static Map<String, Integer> computeActivitiesFrequence(List<MonitoredData> activities) {
		return activities.stream().collect(Collectors.toMap(MonitoredData::getActivity, MonitoredData::get1,
				(oldValue, newValue) -> oldValue + newValue));
	}

	// task 4
	public static Map<Integer, Map<String, Integer>> activityEachDay(List<MonitoredData> activities) {
		return activities.stream().collect(Collectors.groupingBy(MonitoredData::getDay, Collectors
				.toMap(MonitoredData::getActivity, MonitoredData::get1, (oldValue, newValue) -> oldValue + newValue)));
	}

	// task5
	public static Map<String, Long> totalDurationOfEachActivity(List<MonitoredData> activities) {
		return activities.stream()
				.collect(Collectors.toMap(MonitoredData::getActivity,
						p -> Duration.between(p.getStartTime(), p.getEndTime()).toMillis(),
						(oldValue, newValue) -> oldValue + newValue));
	}

	// task6
	public static List<String> avgDurationSmallerThanFiveMin(List<MonitoredData> activities) {
		Map<String, Long> activityCountUnderFiveMin = activities.stream()
				.filter(p -> Duration.between(p.getStartTime(), p.getEndTime()).toMillis() < 300000)
				.collect(Collectors.groupingBy(MonitoredData::getActivity, Collectors.counting()));

		Map<String, Integer> activityFreq = activities.stream().collect(Collectors.toMap(MonitoredData::getActivity,
				MonitoredData::get1, (oldValue, newValue) -> oldValue + newValue));// task3

		return activityCountUnderFiveMin.entrySet().parallelStream()
				.filter(p -> 90 < p.getValue() / activityFreq.get(p.getKey()) * 100).map(p -> p.getKey())
				.collect(Collectors.toList());

	}

}
