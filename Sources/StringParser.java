package com.assignment5;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

public class StringParser {

	public static MonitoredData stringToMonitoredData(String lineString) {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String[] line = lineString.split("\\t\\s");
		String[] activity = line[2].split("\t");
		line[2] = activity[0];

		try {
			Date start = format.parse(line[0]);
			Date end = format.parse(line[1]);
			LocalDateTime localStart = start.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
			LocalDateTime localEnd = end.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
			MonitoredData obj = new MonitoredData(localStart, localEnd, line[2]);
			return obj;
		} catch (ParseException e) {
			e.printStackTrace();
		}

		return null;

	}
}
